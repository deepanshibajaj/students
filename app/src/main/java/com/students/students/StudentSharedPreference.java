package com.students.students;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by deepanshi on 05/07/16.
 */
public class StudentSharedPreference {

    private SharedPreferences studentpref;
    private SharedPreferences.Editor studenteditor;
    // Context
    private Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "studentpref";
    private static final String STUDENTS = "students";

    public StudentSharedPreference(Context context) {
        this._context = context;
        studentpref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        studenteditor = studentpref.edit();
    }

    public void saveStudentsList(String studentlist) {
        studenteditor.putString(STUDENTS, studentlist);
        studenteditor.commit();
    }

    public String getStudentsList() {

        return studentpref.getString(STUDENTS, "");

    }
}
