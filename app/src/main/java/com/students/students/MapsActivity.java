package com.students.students;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String longitude;
    private String latitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        //GET DETAIL FROM STUDENTSLISTHOLDER

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        longitude= bundle.getString("longitude");
        latitude=bundle.getString("latitude");


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        LatLng LATLONG = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));

        //set marker on map
        mMap.addMarker(new MarkerOptions().position(LATLONG).title("Address")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.locationmarker)));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(LATLONG));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LATLONG,13));
        googleMap.animateCamera(CameraUpdateFactory.zoomIn());
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(13), 2000, null);

    }
}
