package com.students.students;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by deepanshi on 05/07/16.
 */

public class StudentsListHolder extends ArrayAdapter<Students> {

    String s_address;
    String s_name;
    int Resource;
    ViewHolder holder = null;
    LayoutInflater vi;
    ArrayList<Students> detailsList;
    Context context;


    public StudentsListHolder(Context context, int resource, ArrayList<Students> students) {
        super(context, resource, students);

        vi = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        Resource = resource;
        detailsList = students;


    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {

        System.out.println("getview:"+position+" "+convertView);
        View v = convertView;



        if (v == null) {
            holder = new ViewHolder();

            v = vi.inflate(Resource, null);

            holder.studentName = (TextView) v.findViewById(R.id.sname);
            holder.studentAddress = (TextView) v.findViewById(R.id.saddress);
            holder.studentEmail = (TextView) v.findViewById(R.id.semailid);
            holder.studentPhoneNumber = (TextView) v.findViewById(R.id.sphonenumber);
            holder.callPhoneNumber = (ImageButton) v.findViewById(R.id.callbtn);
            holder.sendEmail = (ImageButton) v.findViewById(R.id.sendemail);
            holder.setProfileImage = (ImageView) v.findViewById(R.id.setprofileimage);

            v.setTag(holder);

        } else {
            // if holder created, get tag from view
            holder = (ViewHolder) v.getTag();
        }


        holder.studentName.setText(detailsList.get(position).getName());// set student name
        holder.studentAddress.setText(detailsList.get(position).getAddress());// set student address
        holder.studentEmail.setText(detailsList.get(position).getEmailid());// set email id
        holder.studentPhoneNumber.setText(detailsList.get(position).getPhonenumber());// set phone number

        s_address = detailsList.get(position).getAddress();
        s_name = detailsList.get(position).getName();

        String base=detailsList.get(position).getProfileimage();//get bitmap image
        byte[] imageAsBytes = Base64.decode(base.getBytes(), Base64.DEFAULT);
        holder.setProfileImage.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes,0, imageAsBytes.length));//set profile image

        //on click of student address - show address on map
        holder.studentAddress.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {


                String saddress = detailsList.get(position).getAddress();
                GeoCodingLocation locationAddress = new GeoCodingLocation();
                locationAddress.getAddressFromLocation(saddress,
                        vi.getContext(), new GeocoderHandler());


            }
        });

        //on click of student phonenumber button - set number to dialpad
        holder.callPhoneNumber.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                String sphoneumber = detailsList.get(position).getPhonenumber();

                Intent call = new Intent (Intent.ACTION_DIAL, Uri.parse("tel:" + sphoneumber));
                getContext().startActivity(call);

            }
        });


        //on click of student email id button - send mail
        holder.sendEmail.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {


                String semailid = detailsList.get(position).getEmailid();

                Intent emailIntent = new Intent(Intent.ACTION_SEND);

                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{ semailid});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Hi");

                try {
                    vi.getContext().startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                }
                catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getContext(), "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }

            }
        });


        return v;
    }

    static class ViewHolder {
        public TextView studentName;
        public TextView studentAddress;
        public TextView studentEmail;
        public TextView studentPhoneNumber;
        public ImageButton callPhoneNumber;
        public ImageButton sendEmail;
        public ImageView setProfileImage;



    }

    //get longitude and latitude from address
    public class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {

            String longitude;
            String latitude;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    longitude= bundle.getString("longitude");
                    latitude= bundle.getString("latitude");
                    Intent intent1= new Intent(getContext(),MapsActivity.class);
                    intent1.putExtra("longitude",longitude);
                    intent1.putExtra("latitude",latitude);
                    vi.getContext().startActivity(intent1);
                    Log.i("longitude", longitude);
                    Log.i("latitude", latitude);

                    break;
                default:
                    Log.i("locationAddress", null);
            }

        }
    }
}

