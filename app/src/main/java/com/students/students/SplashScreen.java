

	package com.students.students;

	/**
	 *  SPLASH ACTIVITY (FIRST SCREEN IN APP)
	 *  */


	import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;


	public class SplashScreen extends Activity{




		@Override
		protected void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			setContentView(R.layout.splash);


			Handler handler =new Handler();

				handler.postDelayed(new Runnable()
				{



				public void run()
				{

					Intent openMainActivity =  new Intent(SplashScreen.this, GetStudentsList.class);
					startActivity(openMainActivity);

					finish();

				}
			},3000);

			}



		}

