package com.students.students;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by deepanshi on 05/07/16.
 */
public class GetStudentsList extends AppCompatActivity {

    ListView listView;

    private ArrayList<Students> students;
    private StudentSharedPreference sharedPreference;
    private Gson gson;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_student_list);

        //student List
        students = new ArrayList<Students>();
        gson = new Gson();

        //Get data from sharedpreferences
        sharedPreference = new StudentSharedPreference(getApplicationContext());

        //layout list
        listView = (ListView) findViewById(R.id.list);

        //add header ot listview
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.header_addnewstudent, listView,
                false);

        listView.addHeaderView(header, null, false);

        // Get Student Details
        GetStudentDetails();

        // Go to Add New Student
        Button addNewStudent = (Button)header.findViewById(R.id.buttonadd);
        addNewStudent.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view) {

                Intent intent = new Intent(GetStudentsList.this, AddNewStudent.class);
                startActivityForResult(intent,1);
            }
        });



    }

    /**
     * Retrieving data from sharepref
     */
    private void getStudentListFromSharedPreference() {

        //retrieve data from shared preference
        String jsonStudents = sharedPreference.getStudentsList();
        Type type = new TypeToken<List<Students>>(){}.getType();
        students = gson.fromJson(jsonStudents, type);
    }


    private void GetStudentDetails() {

        //set adapter on listview
        if(sharedPreference.getStudentsList().isEmpty()) {

            StudentsListHolder adapter = new StudentsListHolder(GetStudentsList.this, R.layout.student_list_item, students);
            listView.setAdapter(adapter);
        }
        else
        {
            getStudentListFromSharedPreference();
            StudentsListHolder adapter = new StudentsListHolder(GetStudentsList.this, R.layout.student_list_item, students);
            listView.setAdapter(adapter);
        }




            }

    @Override
    public void onBackPressed() {
    }

}
