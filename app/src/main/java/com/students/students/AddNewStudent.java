package com.students.students;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddNewStudent extends AppCompatActivity {

    EditText studentname;
    EditText studentaddress;
    EditText studentemail;
    EditText studentphonenumber;

    TextInputLayout sn;
    TextInputLayout sa;
    TextInputLayout se;
    TextInputLayout sp;

    private ArrayList<Students> studentss;
    private StudentSharedPreference sharedPreference;
    private Gson gson;

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    boolean clicked=false;
    private String userChoosenTask;
    String cameraclicked,imgPath;
    String camerabitmap;
    String camerafilename;
    ImageView imgView;
    String profileimage,filename;
    Bitmap bitmap;
    ImageButton buttonloadimage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);


        //enable back button
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);


        //location permission for marshmallow
        isStoragePermissionGranted();

        //students arraylist
        studentss = new ArrayList<Students>();
        gson = new Gson();

        //get current data from shared preferences
        sharedPreference = new StudentSharedPreference(getApplicationContext());

        //check is sharedpreference is empty else get StudentList From SharedPreference
        if(sharedPreference.getStudentsList().isEmpty()) {

        }
        else
        {
            getStudentListFromSharedPreference();
        }


        //layout
        studentname=(EditText)findViewById(R.id.username);
        studentaddress=(EditText)findViewById(R.id.address);
        studentemail=(EditText)findViewById(R.id.email);
        studentphonenumber=(EditText)findViewById(R.id.Mobilenumber);
        buttonloadimage = (ImageButton) findViewById(R.id.buttonLoadPicture);

        //TextInputLayout
        sn = (TextInputLayout) findViewById(R.id.input_layout_username);
        sa = (TextInputLayout) findViewById(R.id.input_layout_address);
        se = (TextInputLayout) findViewById(R.id.input_layout_email);
        sp = (TextInputLayout) findViewById(R.id.input_layout_Mobilenumber);


        //select Image button
        buttonloadimage.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view) {

                checkPermissions(); //check Permissions granted

                // Create intent to Open Image applications like Gallery, Google Photos
                selectImage();
                clicked=true;

            }
        });

        //validate email id
        studentemail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event){
                if (actionId == EditorInfo.IME_ACTION_NEXT) {

                    final String email = studentemail.getText().toString();
                    if (!isValidEmail(email)) {

                        se.setErrorEnabled(true);
                        se.setError("Invalid Email");
                    }
                    else
                    {

                        studentphonenumber.requestFocus();

                    }

                    return true;
                }
                return false;
            }
        });




    }



    /**
     * Save list of students to own sharedpref
     * @param studentsList
     */
    private void saveStudentListToSharedpreference(ArrayList<Students> studentsList) {
        //convert ArrayList object to String by Gson
        String jsonStudents = gson.toJson(studentsList);

        //save to shared preference
        sharedPreference.saveStudentsList(jsonStudents);
    }

    private void getStudentListFromSharedPreference() {


        studentss = new ArrayList<Students>();
        //retrieve data from shared preference
        String jsonStudents = sharedPreference.getStudentsList();
        Type type = new TypeToken<List<Students>>(){}.getType();
        studentss = gson.fromJson(jsonStudents, type);
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case UtilityPermission.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {

                }
                break;
        }
    }

    //select image from gallery
    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Gallery",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(AddNewStudent.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=UtilityPermission.checkPermission(AddNewStudent.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent(); // image from camera

                } else if (items[item].equals("Choose from Gallery")) {
                    userChoosenTask ="Choose from Gallery";
                    if(result)
                        galleryIntent(); //select image from gallery

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }



    private void galleryIntent()
    {

        Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intenttwo = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intenttwo, REQUEST_CAMERA);

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (resultCode == Activity.RESULT_OK ) {
                if (requestCode == REQUEST_CAMERA) {

                    onCaptureImageResult(data); // method if image picked from camera

                } else if (requestCode == SELECT_FILE) {

                    cameraclicked="0";
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imgPath = cursor.getString(columnIndex);
                    cursor.close();


                    imgView = (ImageView) findViewById(R.id.profileimage);

                    // Set the Image in ImageView after decoding the String
                    imgView.setImageBitmap(BitmapFactory
                            .decodeFile(imgPath));

                    Bitmap icon = BitmapFactory.decodeFile(imgPath);
                    imgView.setImageBitmap(icon);


                }
            }
            else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
                clicked=false;
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }
    }


    private void onCaptureImageResult(Intent data) {

        cameraclicked="1";

        imgView = (ImageView) findViewById(R.id.profileimage);

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        camerafilename =  System.currentTimeMillis() + ".jpg";

        FileOutputStream fo;
        try {

            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
            byte[] byte_arr2 = bytes.toByteArray();
            camerabitmap = Base64.encodeToString(byte_arr2,0);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        imgView.setImageBitmap(thumbnail);

    }


    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    //Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            this.finish();
            return true;

        }

        else if (id == R.id.action_save) {


            if (  ( !studentname.getText().toString().equals("")) &&  ( !studentaddress.getText().toString().equals(""))&&
                    ( !studentemail.getText().toString().equals("")) &&  ( !studentphonenumber.getText().toString().equals("")))
            {


                if (clicked)
                {
                        /* if user took image from camera then cameraclicked=1 else cameraclicked=0 */

                    if(cameraclicked=="0")
                    {

                        String fileNameSegments[] = imgPath.split("/");
                        filename = fileNameSegments[fileNameSegments.length - 1];

                        BitmapFactory.Options options = null;
                        options = new BitmapFactory.Options();
                        options.inSampleSize=2; // reduce size of image by a factor of 2
                        bitmap = BitmapFactory.decodeFile(imgPath,
                                options);

                        ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream2);
                        byte[] byte_arr2 = stream2.toByteArray();
                        profileimage = Base64.encodeToString(byte_arr2,0); //encoded image string
                    }
                    else if(cameraclicked=="1")
                    {
                        profileimage=camerabitmap;
                        filename=camerafilename;
                    }
                }
                      /* check if user clicked select image button else load demo image */

                else if (clicked=false)
                {
                    bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.boy);
                    bitmap = Bitmap.createScaledBitmap(bitmap,
                            250, 250, false);
                    filename="demo.jpg";
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
                    byte[] byte_arr = stream.toByteArray();
                    profileimage = Base64.encodeToString(byte_arr,0);
                }
                else
                {
                    bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.boy);
                    bitmap = Bitmap.createScaledBitmap(bitmap,
                            250, 250, false);
                    filename="demo.jpg";
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
                    byte[] byte_arr = stream.toByteArray();
                    profileimage = Base64.encodeToString(byte_arr,0);
                }


                Students StudentDetails = new Students();
                StudentDetails.setName(studentname.getText().toString());
                StudentDetails.setAddress(studentaddress.getText().toString());
                StudentDetails.setEmailid(studentemail.getText().toString());
                StudentDetails.setPhonenumber(studentphonenumber.getText().toString());
                StudentDetails.setProfileimage(profileimage);

                studentss.add(StudentDetails); //add to students list
                saveStudentListToSharedpreference(studentss); //save to share pref

                Toast.makeText(getApplicationContext(),"Congratulations! Your details saved successfully",
                        Toast.LENGTH_SHORT).show();

                Intent openGetStudentsList=  new Intent(AddNewStudent.this, GetStudentsList.class);
                startActivity(openGetStudentsList);

            }
            else if ( ( studentname.getText().toString().equals("")) )
            {   sn.setErrorEnabled(true);
                sn.setError("Oops ! you left this out");

            }
            else if ( ( studentaddress.getText().toString().equals("")) )
            {   sa.setErrorEnabled(true);
                sa.setError("Oops ! you left this out");

            }
            else if ( ( studentemail.getText().toString().equals("")) )
            {   se.setErrorEnabled(true);
                se.setError("Oops ! you left this out");

            }

            else if ( ( studentphonenumber.getText().toString().equals("")) )
            {   sp.setErrorEnabled(true);
                sp.setError("Oops ! you left this out");

            }

            else
            {
                Toast.makeText(getApplicationContext(),
                        "Some field is empty", Toast.LENGTH_SHORT).show();
            }



            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //permission for marshmallow
    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG","Permission is granted");
                return true;
            } else {

                Log.v("TAG","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG","Permission is granted");
            return true;
        }


    }


    String[] permissions= new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
    };

    private  boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permissions) {
            result = ActivityCompat.checkSelfPermission(getApplicationContext(),p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),1);
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
    }
}
